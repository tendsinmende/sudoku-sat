# Build instructions

# Pre running
- copy a version of "riss" the sat solver into the root directory. The executable **must** be named `riss`
- either provide a `input.txt` with an sodoku or provide a file when executing the program
- install a version of Cargo (the Rust package manager)

# Running/Building
To build the project, execute
```
cargo build --release
```

to build and run execute 
```
cargo run --release
```

or if you don't have an `input.txt` file you can specify it via `FILE` like this
```
cargo run --release FILE
```

or specify another solver with
```
cargo run --release FILE SOLVER
```

If the solution was printed to stdout, the process exits with 10.
If the sudoku has no solution, process exits with 20.

# Notes on the solver

Currently the cnf file generated can be solved by the riss executable and the glucose_static executable. However, Glucoses `glucose.sh` fails to run.
