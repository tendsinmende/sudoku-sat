
use std::option::Option;
use std::fs::{File, OpenOptions, remove_file, rename};
use std::io::*;
use std::thread;
use std::thread::JoinHandle;
use std::marker::Send;
use std::collections::HashSet;
use std::path::Path;

pub enum Literal
{
    Positive( usize ),
    Negative( usize )
}

unsafe impl Send for Literal
{
}

pub type Clause = Vec< Literal >;
pub type Cnf = Vec< Clause >;

pub fn write_literal( file: &mut File, literal: &Literal ) {
    match literal {
        Literal::Positive( x ) => {
            if let Err(er) = write!(file,"{} ", x+1){
                println!("Failed to write line!");
            };
        },
        Literal::Negative( x ) => {
            if let Err(er) = write!(file, "-{} ", x+1){
                println!("Failed to write line!");
            }
        }
    }
}

pub fn write_clause( file: &mut File, clause: &Clause ) {
    for literal in clause.iter() {
        write_literal( file, literal );
    }
}

pub fn write_cnf( file: &mut File, cnf: &Cnf ) {
    for clause in cnf.iter() {
        write_clause( file, clause );

        if let Err(er) = writeln!(file, "0") {}
    }
}


pub struct BufferedCnfWriter {
    pub max_len: usize,
    buf0: Option<Cnf>,
    buf1: Option<Cnf>,
    state: bool,
    file: Option<File>,
    write_thread_handle: Option< JoinHandle< Option<File> > >,
    pub clause_count: usize,
}

impl Drop for BufferedCnfWriter {
    fn drop( &mut self ) {
        self.flush();
        match self.write_thread_handle.take()
        {
            Some(handle) =>
            {
                self.file = handle.join().unwrap().take()
            },
            None => {}
        }
    }
}

impl BufferedCnfWriter {
    pub fn new( max_len: usize, file: File ) -> Self
    {
        BufferedCnfWriter {
            max_len: max_len,
            buf0: None,
            buf1: None,
            state: true,
            file: Some( file ),
            write_thread_handle: None,
            clause_count: 0,
        }
    }

    pub fn flush( &mut self ) {
        match self.write_thread_handle.take()
        {
            Some(handle) =>
            {
                self.file = handle.join().unwrap().take()
            },
            None => {}
        }
        self.swap();

        let buf = self.get_write_buf();
        let mut file = self.file.take().unwrap();
        self.write_thread_handle = Some(thread::spawn(move || {
            write_cnf( &mut file, &buf );
            Some(file)
        }));
    }

    pub fn push( &mut self, clause: Clause ) {
        //Add this clause to the buffer
        self.clause_count += 1;
        let mut buf = self.get_input_buf();
        buf.push( clause );

        if buf.len() > self.max_len {
            if self.state { self.buf0 = Some(buf) }
            else { self.buf1 = Some(buf) }

            self.flush();
        } else
        {
            if self.state { self.buf0 = Some(buf) }
            else { self.buf1 = Some(buf) }
        }
    }

    fn get_input_buf(&mut self) -> Cnf {
        match {
            if self.state { self.buf0.take() }
            else { self.buf1.take() }
        } {
            Some(cnf) => cnf,
            None => Cnf::new()
        }
    }

    fn get_write_buf(&mut self) -> Cnf {
        if self.state { self.buf1.take().unwrap() }
        else { self.buf0.take().unwrap() }
    }

    fn swap(&mut self) {
        self.state = !self.state;
    }

    
}

