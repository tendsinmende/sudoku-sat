
pub mod grid;
pub mod cnf;
pub mod parser;

use std::io::{Read, Write, copy};
use std::fs::*;
use grid::{Cell, Grid, RowIndexIterator, ColumnIndexIterator, SquareIndexIterator};
use cnf::{Literal, BufferedCnfWriter};
use std::time::Instant;
use std::collections::HashSet;
use std::path::Path;
use std::env;

fn main() {
    let mut input_path;
    let mut solver;
    if let Some(arg1) = env::args().nth(1) {
        input_path = arg1;
    }
    else {
        println!("no input specified, using input.txt!");
        input_path = String::from("input.txt");
    }

    if let Some(arg2) = env::args().nth(2) {
        solver = arg2;
    } else {
        println!("no solver specified, using riss!");
        solver = String::from("riss");
    }

    let start_time = Instant::now();

    println!("parse input...");
    let time = Instant::now();
    let grid = parser::parser_file(Path::new(&input_path)).to_grid();
    println!("{:?} (total {:?})", time.elapsed(), start_time.elapsed());

    println!("generating/writing CNF...");
    let time = Instant::now();
    let clause_count = write_cnf( &grid );
    prepend_cnf_info(clause_count, grid.get_width());
    println!("{:?} (total {:?})", time.elapsed(), start_time.elapsed());

    println!("run solver...");
    let time = Instant::now();
    let new_grid = execute_sat(solver, grid);

    println!("{:?} (total {:?})", time.elapsed(), start_time.elapsed());

    //new_grid.print();

    let time = start_time.elapsed();

    println!("write output..");
    new_grid.print_to_file( time );

    println!("total time: {:?}", time);

    for c in new_grid.cells.iter() {
        match c
        {
            Cell::Variable => { println!("no solution found."); ::std::process::exit(20); },
            Cell::Value(_) => {}
        }
    }

    ::std::process::exit(10);
}

///Returns the clause count generated
fn write_cnf( grid: &Grid ) -> usize {
    // if too much RAM is beeing used, lower this number
    let max_clauses = 1000000;
    let mut cnf = BufferedCnfWriter::new( max_clauses, get_new_file());

    // generate used sets
    let number_set = grid.get_number_set();
    let row_used = ( 0 .. grid.get_width() ).map( |row| grid.get_row_set( row ) ).collect::<Vec<HashSet<usize>>>();
    let column_used = ( 0 .. grid.get_width() ).map( |col| grid.get_col_set( col ) ).collect::<Vec<HashSet<usize>>>();
    let square_used = ( 0 .. grid.get_width() ).map( |sq| grid.get_square_set( sq ) ).collect::<Vec<HashSet<usize>>>();

    // für alle Felder
    for row in 0 .. grid.get_width() {
        let row_used = & row_used[ row ];

        for column in 0 .. grid.get_width() {
            let i = grid.index( row, column );

            let column_used = & column_used[ column ];
            let used_set :HashSet<usize> = row_used.union( &column_used ).cloned().collect();

            let square = grid.get_size() * (row / grid.get_size()) + (column / grid.get_size());
            let square_used = & square_used[ square ];
            let used_set :HashSet<usize> = used_set.union( &square_used ).cloned().collect();

            let remaining_set :HashSet<usize> = number_set.difference( &used_set ).cloned().collect();

            match grid.cells[i] {
                Cell::Variable =>
                {
                    // possible values
                    for a in remaining_set.iter() {
                        for b in remaining_set.iter() {
                            if a != b {
                                cnf.push(vec![
                                    Literal::Negative( i + a * grid.get_area() ),
                                    Literal::Negative( i + b * grid.get_area() )
                                ]);
                            }
                        }
                    }

                    // impossible values
                    for z in used_set.iter() {
                        cnf.push(vec![Literal::Negative( i + z*grid.get_area() )]);
                    }
                },
                Cell::Value(z) =>
                {
                    cnf.push(vec![ Literal::Positive( i + z * grid.get_area() ) ]);
                    for a in 0 .. grid.get_width() {
                        if a != z {
                            cnf.push(vec![ Literal::Negative( i + a * grid.get_area() ) ]);
                        }
                    }
                }
            }
        }
    }

    for x in 0 .. grid.get_width() {
        /*
         * Every number at least once per row
         */
        for z in number_set.difference( & row_used[x] ).cloned().collect::<HashSet<usize>>(){
            let mut clause = Vec::new();
            for i in RowIndexIterator::at_row( &grid, x )
            {
                match grid.cells[i] {
                    Cell::Variable =>
                        clause.push(Literal::Positive( i + z * grid.get_area() )),
                    Cell::Value(_) => {}
                }
            }
            cnf.push( clause );
        }

        /*
         * Every number at least once per column
         */
        for z in number_set.difference( & column_used[x] ).cloned().collect::<HashSet<usize>>() {
            let mut clause = Vec::new();
            for i in ColumnIndexIterator::at_column( &grid, x )
            {
                match grid.cells[i] {
                    Cell::Variable =>
                        clause.push(Literal::Positive( i + z * grid.get_area() )),
                    Cell::Value(_) => {}
                }
            }
            cnf.push( clause );
        }

        /*
         * Every number at least once per subgrid
         */
        for z in number_set.difference( & square_used[x] ).cloned().collect::<HashSet<usize>>() {
            let mut clause = Vec::new();
            for i in SquareIndexIterator::at_square( &grid, x )
            {
                match grid.cells[i] {
                    Cell::Variable =>
                        clause.push(Literal::Positive( i + z * grid.get_area() )),
                    Cell::Value(_) => {}
                }
            }
            cnf.push( clause );
        }
    }
    cnf.clause_count
}

///Creates a new file we can write the CNF to.
fn get_new_file() -> File{
    //Test if we already have a file, if yes, delete it.
    if let Ok(_) = File::open("CNF.cnf"){
        std::fs::remove_file("CNF.cnf").expect("Failed to remove already existing CNF!");
    }

    //Open a new cnf file and append out cnf to it.
    let mut file = OpenOptions::new()
        .read(true)
        .write(true)
        .append(true)
        .create_new(true)
        .open("CNF.cnf")
        .expect("Failed to create new CNF file!");

    file
}

//Converts a single number to (index, value) tupel
fn field_to_info(field: u32, area: usize)-> (u32, u32){
    let v = field;
    let i = v % area as u32;
    let z = (v-i) / area as u32;

    (i, z)
}

fn execute_sat(solver: String, grid: Grid) -> Grid{
    use std::process::Command;

    let mut new_grid = grid;

    let output = Command::new(solver)
        .arg("CNF.cnf")
        .output()
        .expect("Failed to execute sat solver!");
    //Now analyse the output
    let out_str = String::from_utf8_lossy(&output.stdout);

    //Now crawl forward line to line till we found the output line.
    for line in out_str.lines(){
        if line.starts_with("v "){
            let mut this_number = String::new();
            let mut this_value = true; //default since there are no + infront of possitive literals

            for ch in line.chars(){
                //Miss the v line
                if ch == 'v' || (ch == ' ' && this_number.len() == 0){
                    continue;
                }
                //Conver the current buffer and start new
                if ch == ' '{
                    //Check if we have to use the info or not
                    if !this_value{
                        //Reset value and buffer
                        this_number = String::new();
                        this_value = true;
                        continue;
                    }else{
                        //Parse the number from the vector and since we should use it, set it in the grid
                        let num :u32 = this_number.parse().expect("error parsing number");
                        let (i, z) = field_to_info(num-1 /* we start counting from zero */, new_grid.get_area());
                        new_grid.cells[i as usize] = Cell::Value(z as usize);

                        //finally reset state
                        this_number = String::new();
                        this_value = true;
                        continue;
                    }
                }

                if ch == '-'{
                    this_value = false;
                    continue;
                }

                //add the current number
                this_number.push(ch);
            }
        }
    }

    new_grid
}


pub fn prepend_cnf_info(clause_count: usize, grid_width: usize){

    let mut file = OpenOptions::new().read(true).write(true).append(true).open("CNF.cnf").expect("Failed to open cnf file for copy");

    let mut src_path = Path::new("CNF.cnf");
    let mut tmp_path = Path::new("tmp_cnf.cnf");
    println!("CNF info: clause count: {}, literal count: {}", clause_count, grid_width.pow(3));
    //Now prepend this info to the cnf by copying the data over to a tmp file, adding our header then copying the tmp file over the last one.
    let mut tmp_file = OpenOptions::new().read(true).write(true).append(true).create(true).open(tmp_path).expect("Failed to create tmp file");
    write!(
        tmp_file, "c generated with love\np cnf {} {}\n",
        grid_width.pow(3), //always using all data
        clause_count
    ).expect("Failed to write header to tmp file");

    //Copy over the data
    std::io::copy(&mut file, &mut tmp_file).expect("Failed to copy tmp cnf to tmp file");
    
    std::fs::remove_file(&src_path).expect("Failed to remove file!");
    std::fs::rename(&tmp_path, &src_path).expect("Failed to rename tmp file to final file");
    //remove_file("tmp_cnf.cnf").expect("Failed to clean up cnf file!");
}
