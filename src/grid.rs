
use std::io::Write;
use std::option::Option;
use std::fs::{File, OpenOptions};
use std::collections::HashSet;
use crate::parser;
#[derive(Clone, Debug)]
pub enum Cell
{
    Variable,
    Value( usize )
}

pub struct Grid
{
    size: usize,
    width: usize,
    area: usize,
    pub cells: Vec<Cell>
}

pub struct GridIterator<'a, IndexIterator: Iterator<Item=usize> >
{
    grid: &'a Grid,
    index_iterator: IndexIterator
}

pub struct RowIndexIterator<'a>
{
    grid: &'a Grid,
    row: usize,
    column: usize
}

pub struct ColumnIndexIterator<'a>
{
    grid: &'a Grid,
    row: usize,
    column: usize
}

pub struct SquareIndexIterator<'a>
{
    grid: &'a Grid,
    square: usize,
    x: usize
}

impl Grid
{
    pub fn new(cells: Vec<Cell>, size: usize) -> Self
    {
        Grid {
            cells: cells,
            size: size,
            width: size.pow(2),
            area: size.pow(4)
        }
    }

    pub fn get_size(&self) -> usize
    {
        self.size
    }

    pub fn get_width(&self) -> usize
    {
        self.width
    }

    pub fn get_area(&self) -> usize
    {
        self.area
    }

    pub fn index(&self, row: usize, column: usize) -> usize
    {
        row * self.get_width() + column
    }

    pub fn index_from_square(&self, square: usize, row: usize, column: usize) -> usize
    {
        self.index(
            row + (square/self.get_size())*self.get_size(),
            column + (square%self.get_size())*self.get_size()
        )
    }

    pub fn into_iter<IndexIterator: Iterator<Item=usize>>(&self, index_iterator: IndexIterator) -> GridIterator< IndexIterator >
    {
        GridIterator
        {
            grid: self,
            index_iterator: index_iterator
        }
    }

    pub fn row_iter(&self, row: usize) -> GridIterator< RowIndexIterator >
    {
        self.into_iter( RowIndexIterator::at_row( self, row ) )
    }

    pub fn column_iter(&self, column: usize) -> GridIterator< ColumnIndexIterator >
    {
        self.into_iter( ColumnIndexIterator::at_column( self, column ) )
    }

    pub fn square_iter(&self, square: usize) -> GridIterator< SquareIndexIterator >
    {
        self.into_iter( SquareIndexIterator::at_square( self, square ) )
    }

    pub fn get_number_set(&self) -> HashSet<usize> {
        ( 0 .. self.get_width() ).collect()
    }

    pub fn get_row_set(&self, row: usize) -> HashSet<usize> {
        let mut nums = HashSet::new();
        for x in self.row_iter(row) {
            match x {
                Cell::Value(v) => {let _ = nums.insert(v);},
                _ => {}
            }
        }
        nums
    }

    pub fn get_remaining_row_set(&self, row: usize) -> HashSet<usize> {
        self.get_number_set().difference( &self.get_row_set( row ) ).cloned().collect()
    }

    pub fn get_col_set(&self, col: usize) -> HashSet<usize> {
        let mut nums = HashSet::new();
        for x in self.column_iter(col) {
            match x {
                Cell::Value(v) => {let _ = nums.insert(v);},
                _ => {}
            }
        }
        nums
    }

    pub fn get_remaining_col_set(&self, col: usize) -> HashSet<usize> {
        self.get_number_set().difference( &self.get_col_set( col ) ).cloned().collect()
    }

    pub fn get_square_set(&self, sq: usize) -> HashSet<usize> {
        let mut nums = HashSet::new();
        for x in self.square_iter(sq) {
            match x {
                Cell::Value(v) => {let _ = nums.insert(v);},
                _ => {}
            }
        }
        nums
    }

    pub fn get_remaining_square_set(&self, sq: usize) -> HashSet<usize> {
        self.get_number_set().difference( &self.get_square_set( sq ) ).cloned().collect()
    }

    pub fn print(&self){
        for i in 0..self.get_area(){
            //New line
            if i % self.get_width() == 0 && i != 0{
                print!("|\n|");
            }else if i % self.get_width() == 0 && i == 0{
                print!("|");
            }

            //print literal
            match self.cells[i]{
                Cell::Value(value) => print!("{} ", value+1 /* we start counting at zero */),
                Cell::Variable => print!("_ "),
            }
        }
        print!("|\n");
    }

    pub fn print_to_file(&self, time: std::time::Duration){
        //Test if we already have a file, if yes, delete it.
        if let Ok(_) = File::open("output.txt"){
            std::fs::remove_file("output.txt").expect("Failed to remove already existing CNF!");
        }

        //Open a new cnf file and append out cnf to it.
        let mut file = OpenOptions::new()
            .write(true)
            .append(true)
            .create_new(true)
            .open("output.txt")
            .expect("Failed to create new CNF file!");

        let character_size = parser::num_u32_characters(self.get_width() as u32) as usize;
        let line_size = (parser::num_u32_characters(self.get_width() as u32) as usize + 1)* self.get_width() //all character + 1x space per character
            + 1 // the first |
            + self.size; // the two chars for the horizontal line

        writeln!(file, "experiment: generator (Time: {:?} s)", time);
        writeln!(file, "number of tasks: 1");
        writeln!(file, "task: 1");
        writeln!(file, "puzzle size: {}x{}", self.get_size(), self.get_size());

        for x in 0..self.get_width(){
            //Check if should add a horizontal line
            if x % self.size == 0{
                for n in 0..self.get_width(){
                    //Check if we should add a +
                    if n % self.size == 0{
                        print!("+-");
                        let _ = write!(file, "+-");
                    }
                    //Add - for this char
                    for _ in 0..character_size+1{
                        print!("-");
                        let _ = write!(file, "-");
                    }
                }

                print!("+\n");
                write!(file, "+\n");
            }

            print!("| ");
            write!(file, "| ");
            for y in 0..self.get_width(){
                //Check if we should add a vertical line
                if y % self.size == 0 && y != 0 && y != self.get_width(){
                    print!("| ");
                    write!(file, "| ");
                }

                //Check how many spaces we need to add.
                let number = match self.cells[self.index(x,y)]{
                    Cell::Value(val) => val+1, //we count with zeros
                    Cell::Variable => {
                        println!("WARNING Trying to write a variable... using 0");
                        0
                    }
                };

                let size = parser::num_u32_characters(number as u32) as usize;
                let mut final_string = String::new();
                for i in 0..(character_size-size){
                    final_string.push(' ');
                }

                //now add the actual number
                final_string.push_str(&number.to_string());
                print!("{} ", final_string);
                if let Err(er) = write!(file, "{} ", final_string){
                    println!("Failed to write out final output");
                }
            }
            print!("|\n");
            let _ = write!(file, "|\n");
        }
        {
            for n in 0..self.get_width(){
                //Check if we should add a +
                if n % self.size == 0{
                    print!("+-");
                    let _ = write!(file, "+-");
                }
                //Add - for this char
                for _ in 0..character_size+1{
                    print!("-");
                    let _ = write!(file, "-");
                }
            }

            print!("+\n");
            write!(file, "+\n");
        }
    }
}

impl<'a, IndexIterator: Iterator<Item=usize>> Iterator for GridIterator<'a, IndexIterator>
{
    type Item = Cell;

    fn next(&mut self) -> Option<Cell>
    {
        let opt = self.index_iterator.next();
        match opt
        {
            Some(i) =>
            {
                if i < self.grid.get_area()
                { Some( self.grid.cells[ i ].clone() ) }
                else { None }
            },
            None => None
        }
    }
}

impl<'a> RowIndexIterator<'a>
{
    pub fn at_row( grid: &'a Grid, row: usize ) -> Self
    {
        RowIndexIterator
        {
            grid: grid,
            row: row,
            column: 0
        }
    }
}

impl<'a> Iterator for RowIndexIterator<'a>
{
    type Item = usize;

    fn next(&mut self) -> Option<usize>
    {
        if self.column < self.grid.get_width()
        {
            let c = self.grid.index(self.row, self.column);
            self.column = self.column + 1;
            Some(c)
        }
        else { None }
    }
}

impl<'a> ColumnIndexIterator<'a>
{
    pub fn at_column( grid: &'a Grid, column: usize ) -> Self
    {
        ColumnIndexIterator
        {
            grid: grid,
            row: 0,
            column: column
        }
    }
}

impl<'a> Iterator for ColumnIndexIterator<'a>
{
    type Item = usize;

    fn next(&mut self) -> Option<usize>
    {
        if self.row < self.grid.get_width()
        {
            let c = self.grid.index(self.row, self.column);
            self.row = self.row + 1;
            Some(c)
        }
        else { None }
    }
}

impl<'a> SquareIndexIterator<'a>
{
    pub fn at_square( grid: &'a Grid, square: usize ) -> Self
    {
        SquareIndexIterator
        {
            grid: grid,
            square: square,
            x: 0
        }
    }
}

impl<'a> Iterator for SquareIndexIterator<'a>
{
    type Item = usize;

    fn next(&mut self) -> Option<usize>
    {
        if self.x < self.grid.get_width()
        {
            let c = self.grid.index(
                self.grid.size * (self.square / self.grid.size) + (self.x / self.grid.size),
                self.grid.size * (self.square % self.grid.size) + (self.x % self.grid.size)
            );
            self.x = self.x + 1;
            Some(c)
        }
        else { None }
    }
}

